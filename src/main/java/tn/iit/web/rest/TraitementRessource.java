package tn.iit.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import tn.iit.dto.TraitementDto;
import tn.iit.service.TraitementService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RequestMapping(value = "/api/traitement")
@RestController()
public class TraitementRessource {

    private final Logger logger = LoggerFactory.getLogger(TraitementRessource.class);
    private final TraitementService traitementService;

    @Autowired
    public TraitementRessource(TraitementService traitementService) {
        this.traitementService = traitementService;
    }

    @GetMapping("{id}")
    public TraitementDto findOne(@PathVariable("id") long id) {
        this.logger.debug("Getting Traitement {}", id);
        return this.traitementService.findOne(id);

    }

    @GetMapping
    public Collection<TraitementDto> findAll(
            @RequestParam(defaultValue = "0") int pageNum,
            @RequestParam(defaultValue = "4") int pageSize
    ) {
        this.logger.debug("Getting all traitements");
        if (pageSize == 0)
            return this.traitementService.findAll();
        else
            return this.traitementService.findAll(PageRequest.of(pageNum, pageSize));
    }

    @PostMapping()
    public TraitementDto add(@Valid @RequestBody TraitementDto traitementDto) {
        this.logger.debug("Adding new Traitement {}", traitementDto);
        return this.traitementService.save(traitementDto);
    }

    @PutMapping()
    public TraitementDto update(@Valid @RequestBody TraitementDto traitementDto) {
        this.logger.debug("Updating Traitement {} with {}", traitementDto.getId(), traitementDto);
        return this.traitementService.save(traitementDto);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") long id) {
        this.logger.debug("Deleting Traitement {}", id);
        this.traitementService.deleteById(id);
    }

    @PostMapping("/deleteByMedicationId")
    public List<TraitementDto> deleteAllTraitementsByMedicationId(@Valid @RequestBody long medicationId) {
        logger.debug("Deleting all Traitements contains the Medicaton {}",medicationId);
        List<TraitementDto> traitementDtos = this.traitementService.deleteAllByMedicationId(medicationId);
        return traitementDtos;
    }
}
