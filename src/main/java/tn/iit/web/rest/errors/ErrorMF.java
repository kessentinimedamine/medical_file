package tn.iit.web.rest.errors;

public class ErrorMF {
    private String message;
    private String conflict;

    public ErrorMF(String message, String conflict) {
        this.message = message;
        this.conflict = conflict;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getConflict() {
        return conflict;
    }

    public void setConflict(String conflict) {
        this.conflict = conflict;
    }

    @Override
    public String toString() {
        return "ErrorMF{" +
                "message='" + message + '\'' +
                ", conflict='" + conflict + '\'' +
                '}';
    }
}
