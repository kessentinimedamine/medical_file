package tn.iit.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.iit.dto.DossierDto;
import tn.iit.dto.PatientDto;
import tn.iit.dto.TraitementDto;
import tn.iit.service.DossierService;
import tn.iit.service.PatientService;
import tn.iit.service.TraitementService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashSet;

@CrossOrigin("*")
@RequestMapping(value = "/api/patient")
@RestController()
public class PatientRessource {

    private final Logger logger = LoggerFactory.getLogger(PatientRessource.class);
    private final PatientService patientService;
    private final TraitementService traitementService;
    private final DossierService dossierService;

    @Autowired
    public PatientRessource(PatientService patientService, TraitementService traitementService, DossierService dossierService) {
        this.patientService = patientService;
        this.traitementService = traitementService;
        this.dossierService = dossierService;
    }

    @GetMapping("{id}")
    public PatientDto findOne(@PathVariable("id") long id) {
        this.logger.debug("Getting Patient {}", id);
        return this.patientService.findOne(id);
    }

    @GetMapping
    public Collection<PatientDto> findAll() {
        this.logger.debug("Getting all Patients");
        return this.patientService.findAll();
    }

    @PostMapping()
    public PatientDto add(@Valid @RequestBody PatientDto patientDto) {
        this.logger.debug("Adding new Patient {}", patientDto);
        return this.patientService.save(patientDto);
    }

    @PutMapping()
    public PatientDto update(@Valid @RequestBody PatientDto patientDto) {
        this.logger.debug("Updating Patient {} with {}", patientDto.getId(), patientDto);
        return this.patientService.save(patientDto);

    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") long id) {
        this.logger.debug("Getting all Dossiers of the Patient{}", id);
        Collection<DossierDto> dossierDtos = this.dossierService.findAllByPatientId(id);

        this.logger.debug("Getting all Traitement of the Patient{}", id);
        HashSet<Collection<TraitementDto>> traitementDtos = new HashSet<Collection<TraitementDto>>();
        if (!dossierDtos.isEmpty()) {
            dossierDtos.forEach(dossierDto -> {
                traitementDtos.add(this.traitementService.findAllByDossierId(dossierDto.getId()));
            });
        }

        this.logger.debug("Deleting All Traitement of the Patient {}", id);
        if (!traitementDtos.isEmpty()) {
            traitementDtos.forEach(traitementDto -> {
                traitementDto.forEach(traitement -> {
                    this.logger.debug("Deleting Traitement {}", traitement.getId());
                    this.traitementService.deleteById(traitement.getId());
                });
            });
        }

        this.logger.debug("Deleting All Dossier of the Patient {}", id);
        if (!dossierDtos.isEmpty()) {
            dossierDtos.forEach(dossierDto -> {
                this.logger.debug("Deleting Dossier {}", dossierDto.getId());
                this.dossierService.deleteById(dossierDto.getId());
            });
        }

        this.logger.debug("Deleting Patient {}", id);
        this.patientService.deleteById(id);
    }
}