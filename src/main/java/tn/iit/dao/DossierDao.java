package tn.iit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.Dossier;

import java.util.Collection;


@Repository
public interface DossierDao extends JpaRepository<Dossier, Long> {

    Collection<Dossier> findAllByPatient_Id(long patient_id);
}