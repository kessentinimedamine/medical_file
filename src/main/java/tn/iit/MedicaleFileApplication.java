package tn.iit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicaleFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicaleFileApplication.class, args);
	}

}
