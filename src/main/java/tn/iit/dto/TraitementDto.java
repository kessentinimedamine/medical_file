package tn.iit.dto;

import javax.validation.constraints.NotNull;

public class TraitementDto {

    private long id;
    @NotNull
    private long dossierId;
    private String medicationName;
    private float medicationDosage;
    private float medicationPrice;
    @NotNull
    private long medicationId;

    public TraitementDto() {
    }

    public TraitementDto(long id, @NotNull long dossierId, String medicationName, float medicationDosage, float medicationPrice, @NotNull long medicationId) {
        this.id = id;
        this.dossierId = dossierId;
        this.medicationName = medicationName;
        this.medicationDosage = medicationDosage;
        this.medicationPrice = medicationPrice;
        this.medicationId = medicationId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDossierId() {
        return dossierId;
    }

    public void setDossierId(long dossierId) {
        this.dossierId = dossierId;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public float getMedicationDosage() {
        return medicationDosage;
    }

    public void setMedicationDosage(float medicationDosage) {
        this.medicationDosage = medicationDosage;
    }

    public float getMedicationPrice() {
        return medicationPrice;
    }

    public void setMedicationPrice(float medicationPrice) {
        this.medicationPrice = medicationPrice;
    }

    public long getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(long medicationId) {
        this.medicationId = medicationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TraitementDto)) return false;

        TraitementDto that = (TraitementDto) o;

        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }

    @Override
    public String toString() {
        return "TraitementDto{" +
                "id=" + id +
                ", dossierId=" + dossierId +
                ", medicationName='" + medicationName + '\'' +
                ", medicationDosage=" + medicationDosage +
                ", medicationPrice=" + medicationPrice +
                ", medicationId=" + medicationId +
                '}';
    }
}
