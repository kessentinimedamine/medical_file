package tn.iit.dto;

import javax.validation.constraints.*;
import java.util.Date;

public class PatientDto {

    private long id;
    @NotNull
    private Date birthday;
    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    @Min(10000000)
    @Max(99999999)
    private long cin;

    public PatientDto() {
    }

    public PatientDto(long id) {
        this.id = id;
    }

    public PatientDto(long id, @NotNull Date birthday, @NotNull @NotEmpty String name, @NotNull @Size(min = 8, max = 8) long cin) {
        this.id = id;
        this.birthday = birthday;
        this.name = name;
        this.cin = cin;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCin() {
        return cin;
    }

    public void setCin(long cin) {
        this.cin = cin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PatientDto)) return false;

        PatientDto that = (PatientDto) o;

        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }

    @Override
    public String toString() {
        return "PatientDto{" +
                "id=" + id +
                ", birthday=" + birthday +
                ", name='" + name + '\'' +
                ", cin=" + cin +
                '}';
    }
}
