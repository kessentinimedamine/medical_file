package tn.iit.dto;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

public class DossierDto {
    @NotNull
    private long id;
    @NotNull
    private Date createdDate;
    @NotNull
    private long patientId;
    private String patientName;
    private Collection<TraitementDto> traitements;


    public DossierDto() {
    }

    public DossierDto(Long id) {
        this.id = id;
    }

    public DossierDto(@NotNull long id, @NotNull Date createdDate, @NotNull long patientId) {
        this.id = id;
        this.createdDate = createdDate;
        this.patientId = patientId;
    }

    @Override
    public String toString() {
        return "DossierDto{" +
                "id=" + id +
                ", createdDate=" + createdDate +
                ", patientId=" + patientId +
                ", patientName='" + patientName + '\'' +
                ", traitements=" + traitements +
                '}';
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public Collection<TraitementDto> getTraitements() {
        return traitements;
    }

    public void setTraitements(Collection<TraitementDto> traitements) {
        this.traitements = traitements;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

}
