package tn.iit.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class MedicationDto {

    private long id;
    @NotNull
    @Size(min = 3)
    @NotEmpty
    private String name;
    @NotNull
    private float dosage;
    @NotNull
    private float price;

    public MedicationDto() {
    }

    public MedicationDto(long id, @NotNull @Size(min = 3) @NotEmpty String name, @NotNull float dosage, @NotNull float price) {
        this.id = id;
        this.name = name;
        this.dosage = dosage;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getDosage() {
        return dosage;
    }

    public void setDosage(float dosage) {
        this.dosage = dosage;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MedicationDto)) return false;

        MedicationDto that = (MedicationDto) o;

        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }

    @Override
    public String toString() {
        return "MedicationDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dosage=" + dosage +
                ", price=" + price +
                '}';
    }
}

