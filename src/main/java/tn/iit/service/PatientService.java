package tn.iit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.dao.PatientDao;
import tn.iit.dto.PatientDto;
import tn.iit.factory.PatientFactory;

import java.util.Collection;

@Transactional
@Service
public class PatientService {

    private final PatientDao patientDao;

    @Autowired
    public PatientService(PatientDao patientDao) {
        this.patientDao = patientDao;
    }

    @Transactional()
    public PatientDto save(PatientDto patientDto) {

        this.patientDao.saveAndFlush(PatientFactory.patientDtoToPatient(patientDto));
        return patientDto;
    }

    @Transactional()
    public void deleteById(Long id) {
        this.patientDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public PatientDto findOne(Long id) {
        return PatientFactory.patientToPatientDto(this.patientDao.getOne(id));
    }

    @Transactional(readOnly = true)
    public Collection<PatientDto> findAll() {
        return PatientFactory.patientsToPatientDtos(this.patientDao.findAll());
    }
}
