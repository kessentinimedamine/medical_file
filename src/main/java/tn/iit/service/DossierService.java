package tn.iit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.dao.DossierDao;
import tn.iit.dto.DossierDto;
import tn.iit.factory.DossierFactory;

import java.util.Collection;

@Transactional
@Service
public class DossierService {
    private final DossierDao dossierDao;
    private final TraitementService traitementService;

    @Autowired
    public DossierService(DossierDao dossierDao, TraitementService traitementService) {
        this.dossierDao = dossierDao;
        this.traitementService = traitementService;
    }

    @Transactional()
    public DossierDto save(DossierDto dossierDto) {
        this.dossierDao.saveAndFlush(DossierFactory.dossierDtoToDossier(dossierDto));
        return dossierDto;
    }

    @Transactional()
    public void deleteById(Long id) {
        this.dossierDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public DossierDto findOne(Long id) {

        DossierDto dossierDto = DossierFactory.dossierToDossierDto(this.dossierDao.getOne(id));
        if (dossierDto != null)
        dossierDto.setTraitements(this.traitementService.findAllByDossierId(id));
        return dossierDto;
    }

    @Transactional(readOnly = true)
    public Collection<DossierDto> findAll() {
        Collection<DossierDto> dossierDtos = DossierFactory.dossiersToDossierDtos(this.dossierDao.findAll());
        if (!dossierDtos.isEmpty()){
            dossierDtos.forEach(dossierDto -> {
                dossierDto.setTraitements(this.traitementService.findAllByDossierId(dossierDto.getId()));
            });
        }
        return dossierDtos;
    }

    public Collection<DossierDto> findAllByPatientId(long patientId) {
        return DossierFactory.dossiersToDossierDtos(this.dossierDao.findAllByPatient_Id(patientId));
    }
}

