package tn.iit.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.dao.TraitementDao;
import tn.iit.dto.MedicationDto;
import tn.iit.dto.TraitementDto;
import tn.iit.entity.Traitement;
import tn.iit.factory.TraitementFactory;
import tn.iit.web.rest.errors.ResourceNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class TraitementService {
    public Logger logger = LoggerFactory.getLogger(TraitementService.class);
    private final TraitementDao traitementDao;
    private final StoreManagementClientService storeManagementClientService;


    @Autowired
    public TraitementService(TraitementDao traitementDao, StoreManagementClientService storeManagementClientService) {
        this.traitementDao = traitementDao;
        this.storeManagementClientService = storeManagementClientService;
    }

    @Transactional()
    public TraitementDto save(TraitementDto traitementDto) {
        this.traitementDao.saveAndFlush(TraitementFactory.traitementDtoToTraitement(traitementDto));
        return traitementDto;
    }

    @Transactional()
    public void deleteById(Long id) {
        this.traitementDao.deleteById(id);
    }

    @Transactional()
    public List<TraitementDto> deleteAllByMedicationId(Long id) {
        List<Traitement> traitements = this.traitementDao.deleteAllByMedicationId(id);
        List<TraitementDto> traitementDtos = new ArrayList<>();
        traitements.forEach(traitement -> {
            traitementDtos.add(TraitementFactory.traitementToTraitementDto(traitement));
        });
        return traitementDtos;
    }


    @Transactional(readOnly = true)
    public TraitementDto findOne(Long id) {
        TraitementDto traitementDto = TraitementFactory.traitementToTraitementDto(this.traitementDao.getOne(id));
        MedicationDto medicationDto = this.storeManagementClientService.getMedicationById(traitementDto.getMedicationId());
        traitementDto.setMedicationDosage(medicationDto.getDosage());
        traitementDto.setMedicationName(medicationDto.getName());
        traitementDto.setMedicationPrice(medicationDto.getPrice());
        return traitementDto;
    }

    @Transactional(readOnly = true)
    public Collection<TraitementDto> findAll(Pageable pageable) {
        Collection<TraitementDto> traitementDtos = TraitementFactory.traitementsToTraitementDtos(this.traitementDao.findAll(pageable).getContent());
        return getMedicationDetails(traitementDtos);
    }

    private Collection<TraitementDto> getMedicationDetails(Collection<TraitementDto> traitementDtos) {
        traitementDtos.forEach(traitementDto -> {
            MedicationDto medicamentDto = this.storeManagementClientService.getMedicationById(traitementDto.getMedicationId());
            traitementDto.setMedicationDosage(medicamentDto.getDosage());
            traitementDto.setMedicationName(medicamentDto.getName());
            traitementDto.setMedicationPrice(medicamentDto.getPrice());
        });
        return traitementDtos;
    }

    @Transactional(readOnly = true)
    public Collection<TraitementDto> findAll() {
        Collection<TraitementDto> traitementDtos = TraitementFactory.traitementsToTraitementDtos(this.traitementDao.findAll());
        return getMedicationDetails(traitementDtos);
    }

    @Transactional(readOnly = true)
    public Collection<TraitementDto> findAllByDossierId(Long id) {
        this.logger.debug("Getting All Traitements By Dossier id {}", id);
        Collection<TraitementDto> traitementDtos = TraitementFactory.traitementsToTraitementDtos(this.traitementDao.findAllByDossier_Id(id));
        List<Long> ids = traitementDtos.stream().map(TraitementDto::getMedicationId).collect(Collectors.toList());
        List<MedicationDto> medicaments = this.storeManagementClientService.getMedicationsByIdList(ids);
        traitementDtos.forEach(traitementDto -> {
            MedicationDto medicamentDto = medicaments.stream()
                    .filter(medicamentDto1 -> medicamentDto1.getId() == traitementDto.getMedicationId())
                    .findFirst()
                    .orElseThrow(() -> new ResourceNotFoundException("NOT FOUND", "Missing Medication with id " + traitementDto.getMedicationId()));
            traitementDto.setMedicationDosage(medicamentDto.getDosage());
            traitementDto.setMedicationName(medicamentDto.getName());
            traitementDto.setMedicationPrice(medicamentDto.getPrice());
        });
        return traitementDtos;

    }
}
